/*window.onload=function(){
	// return;
	setTimeout(function(){
		// document.getElementsByClassName('second')[0].setAttribute('class','second done');
		// document.getElementsByClassName('first')[0].setAttribute('class','first open');
		setTimeout(function(){
			// document.getElementsByClassName('navbar-wrapper')[0].setAttribute('class','navbar-wrapper open');
		},0);
	},500);
}*/
var tiles= document.getElementsByClassName('tile-wrapper');
for(var i = 0;i<tiles.length;i++){
	var tile = tiles[i];
	tile.addEventListener('mouseenter',function mouseenter(){
		var shouldClose = false;
		if(this.getAttribute('class') == 'tile-wrapper expanded'){
			this.setAttribute('class','tile-wrapper');
			shouldClose = true;
		}
		for(var j=0;j<tiles.length;j++){
			var k = tiles[j];
			k.setAttribute('class','tile-wrapper');
		}
		if(!shouldClose){
			this.setAttribute('class','tile-wrapper expanded');
		}
	});
	tile.addEventListener('clicka',function(){
		var shouldClose = false;

		if(this.getAttribute('class').indexOf('expanded') > -1){
			// this.setAttribute('class','tile-wrapper');
			shouldClose = true;
		}
		for(var j=0;j<tiles.length;j++){
			var k = tiles[j];
			// k.setAttribute('class','tile-wrapper');
			if(k.getAttribute('class').indexOf('expanded') > -1){
				k.setAttribute('class','tile-wrapper zIndex');
				setTimeout(function(){
					k.setAttribute('class','tile-wrapper');
				},300);
			}else{
				k.setAttribute('class','tile-wrapper');
			}
		}
		if(!shouldClose){
			this.setAttribute('class','tile-wrapper expanded');
		}
	});
	tile.addEventListener('mouseleave',function(){
		for(var j=0;j<tiles.length;j++){
			var k = tiles[j];
			if(k.getAttribute('class').indexOf('expanded') > -1){
				k.setAttribute('class','tile-wrapper zIndex');				
				setTimeout(function(){
					if(k.getAttribute('class').indexOf('expanded') == -1){
						k.setAttribute('class','tile-wrapper');
					}
				},300);
			}else{
				k.setAttribute('class','tile-wrapper');
			}
		}
	});
}